import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

void main() {
  runApp(const CalcApp());
}

class CalcApp extends StatefulWidget {
  const CalcApp({Key? key}) : super(key: key);

  @override
  State<CalcApp> createState() => _CalcAppState();
}

class _CalcAppState extends State<CalcApp> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Calculator',
        home: CalculatorApp());
  }
}

const Color colorDark = Color(0xFF374352);
const Color colorLight = Color(0xFFe6eeff);

class CalculatorApp extends StatefulWidget {
  const CalculatorApp({Key? key}) : super(key: key);

  @override
  _CalculatorAppState createState() => _CalculatorAppState();
}

class _CalculatorAppState extends State<CalculatorApp> {
  bool darkMode = false;
  String _expression = '';
  String _history = '';

  void clear() {}

  void clearAll() {
    setState(() {
      _history = '';
      _expression = '';
    });
  }

  void numClick(String number) {
    setState(() {
      _expression += number;
    });
  }

  void evaluate() {
    Parser p = Parser();

    Expression exp = p.parse(_expression);

    ContextModel cm = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL, cm);

    setState(() {
      _history = _expression;
      _expression = eval.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: darkMode ? colorDark : colorLight,
        body: SafeArea(
            child: Padding(
          padding: const EdgeInsets.all(18),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          darkMode ? darkMode = false : darkMode = true;
                        });
                      },
                      child: _switchMode()),
                  SizedBox(height: 80),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      _expression,
                      style: TextStyle(
                          fontSize: 55,
                          fontWeight: FontWeight.bold,
                          color: darkMode ? Colors.white : Colors.black),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "=",
                        style: TextStyle(
                            fontSize: 35,
                            color: darkMode ? Colors.green : Colors.grey),
                      ),
                      Text(_history,
                          style: TextStyle(
                              fontSize: 20,
                              color: darkMode ? Colors.green : Colors.grey))
                    ],
                  )
                ],
              )),
              Container(
                  child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        numClick('sin');
                      },
                      child: _buttonOval(title: 'sin'),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        numClick('cos');
                      },
                      child: _buttonOval(title: 'cos'),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        numClick('tan');
                      },
                      child: _buttonOval(title: 'tan'),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        numClick('%');
                      },
                      child: _buttonOval(title: '%'),
                    ),
                  ],
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          clearAll();
                        },
                        child: _buttonRounded(
                            title: 'C',
                            textColor:
                                darkMode ? Colors.green : Colors.redAccent),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('(');
                        },
                        child: _buttonRounded(title: '('),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick(')');
                        },
                        child: _buttonRounded(title: ')'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('/');
                        },
                        child: _buttonRounded(
                            title: '/',
                            textColor:
                                darkMode ? Colors.green : Colors.redAccent),
                      ),
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('7');
                        },
                        child: _buttonRounded(title: '7'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('8');
                        },
                        child: _buttonRounded(title: '8'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('9');
                        },
                        child: _buttonRounded(title: '9'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('*');
                        },
                        child: _buttonRounded(
                            title: 'x',
                            textColor:
                                darkMode ? Colors.green : Colors.redAccent),
                      ),
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('4');
                        },
                        child: _buttonRounded(title: '4'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('5');
                        },
                        child: _buttonRounded(title: '5'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('6');
                        },
                        child: _buttonRounded(title: '6'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('-');
                        },
                        child: _buttonRounded(
                            title: '-',
                            textColor:
                                darkMode ? Colors.green : Colors.redAccent),
                      ),
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('1');
                        },
                        child: _buttonRounded(title: '1'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('2');
                        },
                        child: _buttonRounded(title: '2'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('3');
                        },
                        child: _buttonRounded(title: '3'),
                      ),
                      FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          numClick('+');
                        },
                        child: _buttonRounded(
                            title: '+',
                            textColor:
                                darkMode ? Colors.green : Colors.redAccent),
                      ),
                    ]),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        numClick('0');
                      },
                      child: _buttonRounded(title: '0'),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        numClick(',');
                      },
                      child: _buttonRounded(title: ','),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        clear();
                      },
                      child: _buttonRounded(
                          icon: Icons.backspace_outlined,
                          iconColor:
                              darkMode ? Colors.green : Colors.redAccent),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        evaluate();
                      },
                      child: _buttonRounded(
                          title: '=',
                          textColor:
                              darkMode ? Colors.green : Colors.redAccent),
                    ),
                  ],
                )
              ]))
            ],
          ),
        )));
  }

  Widget _buttonRounded(
      {String? title,
      double padding = 17,
      IconData? icon,
      Color? iconColor,
      Color? textColor}) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: NewContainer(
          darkMode: darkMode,
          borderRadius: BorderRadius.circular(40),
          padding: EdgeInsets.all(padding),
          child: Container(
              width: padding * 2,
              height: padding * 2,
              child: Center(
                  child: title != null
                      ? Text('$title',
                          style: TextStyle(
                              color: textColor != null
                                  ? textColor
                                  : darkMode
                                      ? Colors.white
                                      : Colors.black,
                              fontSize: 30))
                      : Icon(icon, color: iconColor, size: 30)))),
    );
  }

  Widget _buttonOval({String? title, double padding = 17}) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: NewContainer(
          darkMode: darkMode,
          borderRadius: BorderRadius.circular(50),
          padding:
              EdgeInsets.symmetric(horizontal: padding, vertical: padding / 2),
          child: Container(
              width: padding * 2,
              child: Center(
                child: Text(
                  '$title',
                  style: TextStyle(
                      color: darkMode ? Colors.white : Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              )),
        ));
  }

  Widget _switchMode() {
    return NewContainer(
        darkMode: darkMode,
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        borderRadius: BorderRadius.circular(40),
        child: Container(
            width: 70,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.wb_sunny,
                  color: darkMode ? Colors.grey : Colors.redAccent,
                ),
                Icon(
                  Icons.nightlight_round,
                  color: darkMode ? Colors.green : Colors.grey,
                ),
              ],
            )));
  }
}

class NewContainer extends StatefulWidget {
  final bool darkMode;
  final Widget child;
  final BorderRadius borderRadius;
  final EdgeInsetsGeometry padding;

  const NewContainer(
      {Key? key,
      this.darkMode = false,
      required this.child,
      required this.borderRadius,
      required this.padding})
      : super(key: key);

  @override
  _NewContainerState createState() => _NewContainerState();
}

class _NewContainerState extends State<NewContainer> {
  bool _isPressed = false;

  void _onPointerDown(PointerDownEvent event) {
    setState(() {
      _isPressed = true;
    });
  }

  void _onPointerUp(PointerUpEvent event) {
    setState(() {
      _isPressed = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool darkMode = widget.darkMode;
    return Listener(
      onPointerDown: _onPointerDown,
      onPointerUp: _onPointerUp,
      child: Container(
          padding: widget.padding,
          decoration: BoxDecoration(
              color: darkMode ? colorDark : colorLight,
              borderRadius: widget.borderRadius,
              boxShadow: _isPressed
                  ? null
                  : [
                      BoxShadow(
                        color: darkMode
                            ? Colors.black54
                            : Colors.blueGrey.shade200,
                        offset: const Offset(4.0, 4.0),
                        blurRadius: 15.0,
                        spreadRadius: 1.0,
                      ),
                      BoxShadow(
                        color:
                            darkMode ? Colors.blueGrey.shade700 : Colors.white,
                        offset: const Offset(-4.0, -4.0),
                        blurRadius: 15.0,
                        spreadRadius: 1.0,
                      )
                    ]),
          child: widget.child),
    );
  }
}
